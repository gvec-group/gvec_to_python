## Version 1.2.2

* In 1.2.1 the option `flat_eval` was added for marker evaluation.
* Return a numpy contiguous array from `GVEC.transform()`.