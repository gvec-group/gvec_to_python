!================================================================================================================================= !
! PARAMETERFILE 
! execute with
! > ../../build/bin/gvec parameter.ini

!================================================================================================================================= !
! running tests at startup 
!================================================================================================================================= !
  testlevel =-1 ! different levels for testing. -1: off, >0: run tests
    testdbg = F !T: set all tests to failed (to debug tests), F: only failed tests are printed

!================================================================================================================================= !
! compute initial solution from existing equilibrium
!================================================================================================================================= !

  whichInitEquilibrium = 0 ! 0: only from input parameters, 1: from VMEC netcdf file, 2: from internally generated Soloview
  
  ProjectName = CIRC_TOK

  init_fromBConly      = T ! true: only use axis and boundary for X1,X2 (True is default)
                      ! false: only needed if VMEC data is used, interpolate on full mesh s=0...
  init_LA              = T !T: compute lambda from initial mapping (default),  F: lambda=0 / from VMEC at initialization


!================================================================================================================================= !
! visualization parameters
!================================================================================================================================= !
which_visu = 0          ! 0: visualize gvec data, 1: visualize vmec data (whichInitEquilibrium=1)
visu1D       = 0 !12       ! 0: off, >0:  write 1D data of all modes of R,Z,lambda over the flux / radial coordinate
  np_1d      = 5 
visu2D       = 2       ! 0: off, 1: write paraview file of boundary, 2: write paraview file for zeta=const. planes 
  np_visu_BC     =   (/80,80/)

  np_visu_planes = (/3,40,2/)
  visu_planes_min = (/0.,0.,0. /)  ! range of visualization for s,theta,zeta: min in [0,1] 
  visu_planes_max = (/1.,1.,0.5/)  !                                          max in [0,1]
visu3D       = 0        ! 0: off, 1: write 3D paraview file 
  np_visu_3D  = (/2,40,40/)
  visu_3D_min = (/0.,0.,0. /)  ! range of visualization for s,theta,zeta: min in [0,1] 
  visu_3D_max = (/1.,1.,0.2/)  !                                          max in [0,1]

  SFL_theta = F
!================================================================================================================================= !
! grid in s direction
!================================================================================================================================= !

  sgrid_nElems=51
  sgrid_grid_type=4  ! 0: uniform, 1: sqrt (finer at edge), 2: s^2 (finer at center), 3: bump (fine at edge and center)
!================================================================================================================================= !
! 1D spline BCs at axis ... 1:BC_OPEN,2:BC_NEUMANN,3:BC_DIRICHLET,4:BC_SYMM,5:BC_SYMMZERO,6:BC_ANTISYMM
!================================================================================================================================= !
                                 !  default    , stronger
 !X1X2_BCtype_axis_mn_zero    = 4 ! BC_NEUMANN  , BC_SYMM     
 !X1X2_BCtype_axis_m_zero     = 4 ! BC_NEUMANN  , BC_SYMM     
 !X1X2_BCtype_axis_m_odd_first= 6 ! BC_DIRICHLET, BC_ANTISYMM 
 X1X2_BCtype_axis_m_odd      = 6 ! BC_DIRICHLET, BC_ANTISYMM 
 X1X2_BCtype_axis_m_even     = 5 ! BC_DIRICHLET, BC_SYMMZERO
 !LA_BCtype_axis_mn_zero      = 3 ! BC_DIRICHLET, BC_SYMMZERO
 !LA_BCtype_axis_m_zero       = 2 ! BC_NEUMANN  , BC_SYMM    
 !LA_BCtype_axis_m_odd_first  = 3 ! BC_DIRICHLET, BC_ANTISYMM
 LA_BCtype_axis_m_odd        = 6 ! BC_DIRICHLET, BC_ANTISYMM
 LA_BCtype_axis_m_even       = 5 ! BC_DIRICHLET, BC_SYMMZERO

!================================================================================================================================= !
! discretization parameters
!================================================================================================================================= !

  degGP  = 7               ! number of gauss points per radial element
  
  fac_nyq = 4              ! number of points for integration in (theta,zeta), :mn_nyq=fac_nyq * max(mn_max (all variables)) 
   
  X1X2_deg  = 5            !polynomial degree in radial discretization for X1 and X2 variable
  X1_mn_max = (/10, 0/)   !maximum mumber of fourier modes (mmax,nmax)
  X2_mn_max = (/10, 0/)   !maximum mumber of fourier modes (mmax,nmax)
  
  X1_sin_cos = _cos_       !which fourier modes: can be either _SIN_,_COS_, or _sin_cos_
  X2_sin_cos = _sin_       !
  
  LA_deg    = 4            ! polynomial degree in radial discretization for Lambda variable
  LA_continuity = 3
  LA_mn_max = (/10, 0/)   ! maximum mumber of fourier modes (mmax,nmax)
  LA_sin_cos = _sin_       !


which_hmap = 1           ! type of global mapping h between (X1,X1,zeta)->(x,y,z), 1: R=X1,Z=X2 torus, 2: cylinder, 10: knot
  hmap_cyl_len=20.       ! for cylinder map
!================================================================================================================================= !
! fourier modes at the edge boundary condition
! FORMAT AA_X_ccc( M; N) with AA: X1,X2 , X=a/b (axis/edge) 
!                             ccc: cos/sin, ( M; N) without NFP with any number of whitespaces: (  0;  0) ( 1; 0) ( -1; -2) 
! modes which are not specified here are set to zero!!!
!================================================================================================================================= !
X1_b_cos(  0;  0) =  10.0 

X1_b_cos(  1;  0) =  1.0 
X2_b_sin(  1;  0) =  1.0


!axis only used for initialization:
X1_a_cos(  0;  0) = 10.00

!================================================================================================================================= !
! profiles
!================================================================================================================================= !
! fitting polynomial to iota=1/q, with q=1.71 + 0.16*Phi_norm
iota_coefs= (/0.58476912099726 , -0.054444275875356 , 0.00386977213581 , 0.0021159332275484 , -0.0024164370588146 , 0.00086376790426014/) !(a,b,c,d,...) : iota(phi_norm)=a+b*phi_norm+c*phi_norm^2 ...             phi_norm: normalized tor.flux
! sign_iota = -1 !default

pres_coefs= (/ 1.0, 0. /)
pres_scale= 6409.96533 !8.055e-3/mu_0  


PHIEDGE    = -9.4458485                ! torodial flux at the last flux surface 
!================================================================================================================================= !
! iteration
!================================================================================================================================= !
MaxIter =10000          ! maximum number of iterations
start_dt=0.9            ! first timestep, is automatically adapted
minimize_tol=1.0e-12   ! absolute tolerance |Force|*dt<abstol
outputIter = 1000          ! number of iterations after which a state & visu is written
logIter = 100              ! write log of iterations to screen 

PrecondType = 1 ! -1: off(default), 1: on
MinimizerType = 10 ! 0: descent(default), 10: accel. Gradient descent
