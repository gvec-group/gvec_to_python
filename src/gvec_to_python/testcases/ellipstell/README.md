Example of of a simple 3D stellarator case, run with GVEC for 200000 iterations.

each directory is a run from GVEC with a input parameterfile and the final output/restart file of GVEC.

Case:
* `newBC_E1D6_M6N6`: run with 1 radial element of degree 6 and maximum mode numbers `m_max=6,n_max=6`, "newBC" means using the new smoothness constraint at the axis.
* `newBC_E4D6_M6N6`: run with 4 radial element of degree 6 and maximum mode numbers `m_max=6,n_max=6` (restarted from E1 case)


